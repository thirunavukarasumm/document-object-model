// Creating elements


let ul = document.querySelector("ul")
let li = document.createElement("li")

// Adding content into the list
li.innerHTML = "Cold Coffee"
// Appending the element into the ul
ul.append(li)
//  Changes the original value as this is in global scope
li.innerHTML = "Coldoffee"

//  Modify texts

let listItems = document.querySelector("[class = 'list-items']")
console.log(listItems.innerText)
console.log(listItems.innerHTML)
console.log(listItems.textContent)

listItems.innerText = "Milkshake"


//  Modifying attributes 

listItems.setAttribute('id',"test");
listItems.removeAttribute('id');

console.log(listItems.getAttribute('tag'));

//  Modifying classes

listItems.classList.add("test")
listItems.classList.remove("test")

console.log(listItems.classList.contains("test"))


//  Remove elements

// listItems.remove() // Removes the element completely

listItems.removeAttribute("tag") // Removes attrs

